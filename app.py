from __future__ import print_function
import base64
import hashlib
import json
from cassandra.cluster import Cluster
from cassandra.auth import PlainTextAuthProvider
import logging
import os

logger = logging.getLogger()

logger.setLevel(os.environ['log_level'])


def lambda_handler(event, context):
    for record in event['Records']:
        data = base64.b64decode(record["kinesis"]["data"])
        stream_data = json.loads(data)
        payload = stream_data['payload']
        key = hashlib.md5(str(json.dumps(stream_data['key'])).encode()).hexdigest()
        logger.debug("Insert Message to Cassandra: " + str(data))
        try:
            auth_provider = PlainTextAuthProvider(username=os.environ['cassandra_login'], password=os.environ['cassandra_password'])
            cluster = Cluster([os.environ['cassandra_host']], auth_provider=auth_provider)
            session = cluster.connect()
            logger.debug("Successfully connected to Cassandra")
            session.set_keyspace(os.environ['cassandra_keyspace'])
            payload.update({'id': key})
            if stream_data['action'] == 'insert':
                logger.info("inserting message")
                session.execute(
                    "INSERT INTO {0} (id, test1) VALUES ('{1}', '{2}')".format(os.environ['cassandra_table'], payload['id'],
                                                                               payload['test1']))
                logger.debug("Successfully updated message to Cassandra")
            elif stream_data['action'] == 'delete':
                session.execute("DELETE FROM test WHERE id=%s", [key])
                logger.debug("Successfully removed message to Cassandra")
            elif stream_data['action'] == 'update':
                session.execute("""UPDATE test set test1=%(test1)s WHERE id=%(id)s""", payload)
                logger.debug("Successfully updated message to Cassandra")

            return True
        except Exception as err:
            logger.error("Failed writing to cassandra: {0}".format(err))
            return False
