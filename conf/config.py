import os
import json


def get_config():
    filename = os.path.join(
        os.path.dirname(__file__), 'config.json')
    with open(filename) as f:
        config = json.load(f)
        return config
