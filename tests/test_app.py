import boto3
import unittest
import json
from conf import config


class AppTestCase(unittest.TestCase):
    def setUp(self):
        self.conf = config.get_config()

    def test_insert_lambda(self):
        client = boto3.client('lambda', region_name=self.conf['region'])
        with open("./insert.json", "rb") as binary_file:
            payload = binary_file.read()
        response = client.invoke(FunctionName=self.conf['function_name'], Payload=payload)
        self.assertEqual(response['ResponseMetadata']['HTTPStatusCode'], 200)

    def test_update_lambda(self):
        client = boto3.client('lambda', region_name=self.conf['region'])
        with open("./update.json", "rb") as binary_file:
            payload = binary_file.read()
        response = client.invoke(FunctionName=self.conf['function_name'], Payload=payload)
        self.assertEqual(response['ResponseMetadata']['HTTPStatusCode'], 200)

    def test_delete_lambda(self):
        client = boto3.client('lambda', region_name=self.conf['region'])
        with open("./delete.json", "rb") as binary_file:
            payload = binary_file.read()
        response = client.invoke(FunctionName=self.conf['function_name'], Payload=payload)
        self.assertEqual(response['ResponseMetadata']['HTTPStatusCode'], 200)

    def test_stream(self):
        client = boto3.client('kinesis', region_name=self.conf['region'])
        data = [{
            "action": "insert",
            "key": {
                "fieldA": "valueA",
                "fieldB": "valueB"
            },
            "payload": {
                "test1": "value1"
            }
        }]

        response = client.put_record(
            StreamName=self.conf['stream_name'],
            Data=json.dumps(data),
            PartitionKey='test-key'
        )
        self.assertEqual(response['ShardId'], 'shardId-000000000000')
