#Receive Kinesis message and update cassandra

## Install
```bash
pip3.6 install -r requirements.txt
```
## Create kinesis stream
```bash
python3.6 create-stream.py 1 us-east-2
```

##Create or update lambda function
First, you must create an IAM Role on your AWS account called lambda_basic_execution with the LambdaBasicExecution policy attached.

Write down the role ARN to ```conf\config.json```

Next, creates or updates lambda function and create association between a stream source and a Lambda function.

```bash
python3.6 deploy-lambda.py
```

###Lambda request payload
Lambda function will accept the following request format from Kinesis Stream:
```json
{
  "action": "insert"  | "update" | "delete",
  "key": {
    "fieldA": "valueA",
    "fieldB": "valueB"
  },
  "payload": {
    "test1": "value1"
  }
}

```
##Cassandra structure
run CQL query for creating table

```./samples/table.cql```
##Unit tests
```bash
python3.6 -m unittest discover -s tests
```
