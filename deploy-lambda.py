import datetime as dt
import os
import zipfile
from shutil import copyfile
from shutil import copytree
from tempfile import mkdtemp
import json
import boto3
import pip

from conf import config

CURRENT_DIR = os.getcwd()


def archive(src, dest, filename):
    output = os.path.join(dest, filename)
    zfh = zipfile.ZipFile(output, 'w', zipfile.ZIP_DEFLATED)

    for root, _, files in os.walk(src):
        for file in files:
            zfh.write(os.path.join(root, file))
    zfh.close()
    return os.path.join(dest, filename)


def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def read(path, loader=None, binary_file=False):
    open_mode = 'rb' if binary_file else 'r'
    with open(path, mode=open_mode) as fh:
        if not loader:
            return fh.read()
        return loader(fh.read())


def remove_old_zip_files(folder):
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)


def timestamp(fmt='%Y-%m-%d-%H%M%S'):
    now = dt.datetime.utcnow()
    return now.strftime(fmt)


def _install_packages(path, packages):
    def _filter_blacklist(package):
        blacklist = ['-i', '#', 'Python==', 'python-lambda==']
        return all(package.startswith(entry) is False for entry in blacklist)

    filtered_packages = filter(_filter_blacklist, packages)
    for package in filtered_packages:
        if package.startswith('-e '):
            package = package.replace('-e ', '')

        print('Installing {package}'.format(package=package))
        pip.main(['install', package, '-t', path, '--ignore-installed'])


def get_client(client, region=None):
    """Shortcut for getting an initialized instance of the boto3 client."""

    return boto3.client(client, region_name=region)


def build(src):
    path_to_dist = os.path.join(src, 'dist')
    mkdir(path_to_dist)

    path_to_temp = mkdtemp(prefix='aws-lambda')
    pip_install_to_target(path_to_temp)
    files = [os.path.join(src, 'app.py'), os.path.join(src, 'conf')]

    # "cd" into `temp_path` directory.
    os.chdir(path_to_temp)
    for f in files:
        if os.path.isfile(f):
            _, filename = os.path.split(f)
            # Copy handler file into root of the packages folder.
            copyfile(f, os.path.join(path_to_temp, filename))
        elif os.path.isdir(f):
            destination_folder = os.path.join(path_to_temp, f[len(src) + 1:])
            copytree(f, destination_folder)

    output_filename = '{0}-{1}.zip'.format(timestamp(), 'deploy')
    path_to_dist = os.path.join(src, 'dist')
    remove_old_zip_files(path_to_dist)
    path_to_zip_file = archive('./', path_to_dist, output_filename)
    return path_to_zip_file


def get_role_name(account_id, role):
    """Shortcut to insert the `account_id` and `role` into the iam string."""
    prefix = 'aws'
    return 'arn:{0}:iam::{1}:role/{2}'.format(prefix, account_id, role)


def get_account_id(region=None):
    """Query STS for a user's account_id"""
    client = get_client('sts', region)
    return client.get_caller_identity().get('Account')


def pip_install_to_target(path):
    packages = []
    if os.path.exists('lambda-requirements.txt'):
        print('Gathering lambda-requirement packages')
        data = read('lambda-requirements.txt')
        packages.extend(data.splitlines())

    _install_packages(path, packages)


def function_exists(cfg, function_name):
    client = boto3.client('lambda', region_name=cfg['region'])

    functions = []
    functions_resp = client.list_functions()
    functions.extend([
        f['FunctionName'] for f in functions_resp.get('Functions', [])
    ])
    while 'NextMarker' in functions_resp:
        functions_resp = client.list_functions(
            Marker=functions_resp.get('NextMarker'),
        )
        functions.extend([
            f['FunctionName'] for f in functions_resp.get('Functions', [])
        ])
    return function_name in functions


def create_function(cfg, path_to_zip_file):
    """Register and upload a function to AWS Lambda."""

    print('Creating your new Lambda function')
    byte_stream = read(path_to_zip_file, binary_file=True)

    account_id = get_account_id(cfg['region'])
    role = get_role_name(account_id, cfg.get('lambda_role', 'lambda_basic_execution'))

    client = get_client('lambda', cfg['region'])

    print('Creating lambda function with name: {}'.format(cfg.get('function_name')))

    kwargs = {
        'FunctionName': cfg['function_name'],
        'Runtime': cfg.get('runtime', 'python3.6'),
        'Role': role,
        'Handler': cfg['lambda_handler'],
        'Code': {'ZipFile': byte_stream},
        'Description': cfg.get('description'),
        'Timeout': cfg.get('timeout', 60),
        'MemorySize': cfg.get('memory_size', 512),
        'Publish': True,
        'Environment': get_environment_variables(cfg)
    }
    client.create_function(**kwargs)

    kinesis = get_client('kinesis', cfg['region'])
    stream = kinesis.describe_stream(
        StreamName=cfg['stream_name']
    )
    stream_arn = stream['StreamDescription']['StreamARN']

    print('Creating association between a stream source and a Lambda function')
    client.create_event_source_mapping(EventSourceArn=stream_arn, FunctionName=cfg['function_name'], Enabled=True,
                                       StartingPosition='TRIM_HORIZON')


def get_environment_variables(cfg):
    return {
        'Variables': {
            'log_level': cfg['log_level'],
            'cassandra_host': cfg['cassandra_host'],
            'cassandra_login': cfg['cassandra_login'],
            'cassandra_password': cfg['cassandra_password'],
            'cassandra_keyspace': cfg['cassandra_keyspace'],
            'cassandra_table': cfg['cassandra_table']
        }
    }


def update_function(cfg, path_to_zip_file):
    """Updates the code of an existing Lambda function"""

    print('Updating your Lambda function')
    byte_stream = read(path_to_zip_file, binary_file=True)
    client = boto3.client('lambda', region_name=cfg['region'])
    client.update_function_code(
        FunctionName=cfg['function_name'],
        ZipFile=byte_stream,
        Publish=True,
    )
    client.update_function_configuration(FunctionName=cfg['function_name'], Environment=get_environment_variables(cfg))
    kinesis = get_client('kinesis', cfg['region'])
    stream = kinesis.describe_stream(
        StreamName=cfg['stream_name']
    )
    stream_arn = stream['StreamDescription']['StreamARN']

    source_mappings = client.list_event_source_mappings(
        EventSourceArn=stream_arn,
        FunctionName=cfg['function_name'],
    )
    if len(source_mappings['EventSourceMappings']) <= 0:
        print('Creating association between a stream source and a Lambda function')
        client.create_event_source_mapping(EventSourceArn=stream_arn, FunctionName=cfg['function_name'], Enabled=True,
                                           StartingPosition='TRIM_HORIZON')


def create_lambda_role(cfg):
    policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "Service": "lambda.amazonaws.com"
                },
                "Action": "sts:AssumeRole"
            }
        ]
    }
    client = get_client('iam')
    client.create_role(
        AssumeRolePolicyDocument=json.dumps(policy),
        Path='/',
        RoleName=cfg['lambda_role'],
    )
    client.attach_role_policy(
        PolicyArn='arn:aws:iam::aws:policy/service-role/AWSLambdaKinesisExecutionRole',
        RoleName=cfg['lambda_role']
    )


def deploy(src, cfg):
    path_to_zip_file = build(src)
    print("Creating lambda role")
    # create_lambda_role(cfg)
    if function_exists(cfg, cfg['function_name']):
        update_function(cfg, path_to_zip_file)
    else:
        create_function(cfg, path_to_zip_file)


try:
    settings = config.get_config()
    deploy(CURRENT_DIR, settings)
except Exception as err:
    print("Error: {}".format(err))
