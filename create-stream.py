import boto3
import sys
from conf import config

if len(sys.argv) != 3:
    print("Please provide at least one argument for shard count and optionally for region name")
    exit()

shard_count, region_name = sys.argv[1:]
settings = config.get_config()

client = boto3.client('kinesis', region_name=region_name)
try:
    response = client.create_stream(
        StreamName=settings['stream_name'],
        ShardCount=int(shard_count)
    )

    print(response)
except Exception as err:
    print("Error: {}".format(err))
